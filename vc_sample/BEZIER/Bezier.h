// Bezier.h: interface for the CBezier class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BEZIER_H__EC86F1E4_D977_11D5_BE3B_0004764652A5__INCLUDED_)
#define AFX_BEZIER_H__EC86F1E4_D977_11D5_BE3B_0004764652A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <afxwin.h>
#include <math.h>

#define MAX_BEZIER_POINT  255
#define BEZIER_STEP		  38
class CBezier
{
public:
	void Move(int offx,int offy);
	void setPoint(int index,CPoint point);
	void Rotate(CPoint startPoint,CPoint endPoint);
	BOOL PtInBezier(CPoint point);
	void Draw(CDC *dc);
	CBezier();
	virtual ~CBezier();
	CPoint m_Point[MAX_BEZIER_POINT];
	double p[7][3];

	CPoint m_Center;
	UINT   m_nPoint;
	void CalcCenter();
private:
	CPoint hor(int degree,CPoint *point,double t);	
	CPoint decas(int degree,CPoint *point,double t);		
};

#endif // !defined(AFX_BEZIER_H__EC86F1E4_D977_11D5_BE3B_0004764652A5__INCLUDED_)
