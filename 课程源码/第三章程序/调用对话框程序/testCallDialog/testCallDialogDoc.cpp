// testCallDialogDoc.cpp : implementation of the CTestCallDialogDoc class
//

#include "stdafx.h"
#include "testCallDialog.h"

#include "testCallDialogDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogDoc

IMPLEMENT_DYNCREATE(CTestCallDialogDoc, CDocument)

BEGIN_MESSAGE_MAP(CTestCallDialogDoc, CDocument)
	//{{AFX_MSG_MAP(CTestCallDialogDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogDoc construction/destruction

CTestCallDialogDoc::CTestCallDialogDoc()
{
	// TODO: add one-time construction code here

}

CTestCallDialogDoc::~CTestCallDialogDoc()
{
}

BOOL CTestCallDialogDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogDoc serialization

void CTestCallDialogDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogDoc diagnostics

#ifdef _DEBUG
void CTestCallDialogDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTestCallDialogDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogDoc commands
