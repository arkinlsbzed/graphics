#if !defined(AFX_INPUTDIALOG3_H__015412F6_20ED_4136_ACC7_A3099320C90E__INCLUDED_)
#define AFX_INPUTDIALOG3_H__015412F6_20ED_4136_ACC7_A3099320C90E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InputDialog3.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInputDialog3 dialog

class CInputDialog3 : public CDialog
{
// Construction
public:
	CInputDialog3(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CInputDialog3)
	enum { IDD = IDD_DIALOG3 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInputDialog3)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CInputDialog3)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INPUTDIALOG3_H__015412F6_20ED_4136_ACC7_A3099320C90E__INCLUDED_)
