// p3_5a.h : main header file for the P3_5A application
//

#if !defined(AFX_P3_5A_H__11D03538_DF37_46FC_977D_9AC437C3C3A1__INCLUDED_)
#define AFX_P3_5A_H__11D03538_DF37_46FC_977D_9AC437C3C3A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CP3_5aApp:
// See p3_5a.cpp for the implementation of this class
//

class CP3_5aApp : public CWinApp
{
public:
	CP3_5aApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CP3_5aApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CP3_5aApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_P3_5A_H__11D03538_DF37_46FC_977D_9AC437C3C3A1__INCLUDED_)
