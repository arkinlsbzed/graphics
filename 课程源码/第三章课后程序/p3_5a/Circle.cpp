// Circle.cpp: implementation of the CCircle class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "p3_5a.h"
#include "Circle.h"
#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCircle::CCircle()
{

}

CCircle::~CCircle()
{

}
double Round(double val)
{
    return (val> 0.0) ? floor(val+ 0.5) : ceil(val- 0.5);
}
void CCircle::drawCircle(CDC * pDC,double m,double n,double r)
{

	int x = 0;
	int y=Round(r);
	double e=0;
	for(x = 0;x<=Round(r/sqrt(2));x++)
	{

		
		double ux=x;
		double uy=y;

		double dx=x;
		double dy=y-1;

		pDC->SetPixelV(Round(dx)+m,Round(dy)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(Round(ux)+m,Round(uy)+n,RGB(e*255,e*255,e*255));

		pDC->SetPixelV(Round(dy)+m,Round(dx)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(Round(uy)+m,Round(ux)+n,RGB(e*255,e*255,e*255));

		pDC->SetPixelV(Round(dy)+m,-Round(dx)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(Round(uy)+m,-Round(ux)+n,RGB(e*255,e*255,e*255));

		pDC->SetPixelV(Round(dx)+m,-Round(dy)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(Round(ux)+m,-Round(uy)+n,RGB(e*255,e*255,e*255));
		
		pDC->SetPixelV(-Round(dx)+m,-Round(dy)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(-Round(ux)+m,-Round(uy)+n,RGB(e*255,e*255,e*255));

		pDC->SetPixelV(-Round(dy)+m,-Round(dx)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(-Round(uy)+m,-Round(ux)+n,RGB(e*255,e*255,e*255));

		pDC->SetPixelV(-Round(dy)+m,Round(dx)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(-Round(uy)+m,Round(ux)+n,RGB(e*255,e*255,e*255));

		pDC->SetPixelV(-Round(dx)+m,Round(dy)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(-Round(ux)+m,Round(uy)+n,RGB(e*255,e*255,e*255));

		e=e-(-x/sqrt(r*r-x*x));
		if(e>=1.0)
		{
			y=y-1;
			e=e-1;
		}
	}

}