// p3_2Doc.h : interface of the CP3_2Doc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_P3_2DOC_H__B12C97E2_3372_424A_8547_E71E82EE6AF0__INCLUDED_)
#define AFX_P3_2DOC_H__B12C97E2_3372_424A_8547_E71E82EE6AF0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CP3_2Doc : public CDocument
{
protected: // create from serialization only
	CP3_2Doc();
	DECLARE_DYNCREATE(CP3_2Doc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CP3_2Doc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CP3_2Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CP3_2Doc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_P3_2DOC_H__B12C97E2_3372_424A_8547_E71E82EE6AF0__INCLUDED_)
