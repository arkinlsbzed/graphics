// TestView.cpp : implementation of the CTestView class
//

#include "stdafx.h"
#include "Test.h"

#include "TestDoc.h"
#include "TestView.h"
#define ROUND(a) int(a+0.5)
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	//{{AFX_MSG_MAP(CTestView)
	ON_COMMAND(ID_MSTYLE, OnMstyle)
	ON_COMMAND(ID_MLAY, OnMlay)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestView construction/destruction

CTestView::CTestView()
{
	// TODO: add construction code here
	HalfEdge=100;
	P=new CP2[13];//中心点;
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView drawing

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here	
}

/////////////////////////////////////////////////////////////////////////////
// CTestView printing

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestView diagnostics

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestView message handlers

void CTestView::DrawObject()//绘制地砖
{
	CRect Rect;
	GetClientRect(&Rect);
	CDC *pDC=GetDC();
	pDC->SetMapMode(MM_ANISOTROPIC);//自定义坐标系
	pDC->SetWindowExt(Rect.Width(),Rect.Height());
	pDC->SetViewportExt(Rect.Width(),-Rect.Height());//x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(Rect.Width()/2,Rect.Height()/2);//屏幕中心为原点
	CPen NewPen,*pOldPen; 
	NewPen.CreatePen(PS_SOLID,3,RGB(203,151,76)); 
	pOldPen=pDC->SelectObject(&NewPen);
	CBrush NewBrush,*pOldBrush;
	NewBrush.CreateSolidBrush(RGB(221,168,92));
	pOldBrush=pDC->SelectObject(&NewBrush);
	pDC->Rectangle(ROUND(P[1].x),ROUND(P[1].y),ROUND(P[3].x),ROUND(P[3].y));
	pDC->MoveTo(ROUND(P[1].x),ROUND(P[1].y));//左上
	pDC->LineTo(ROUND(P[2].x),ROUND(P[2].y));//左下
	pDC->LineTo(ROUND(P[3].x),ROUND(P[3].y));//右下
	pDC->LineTo(ROUND(P[4].x),ROUND(P[4].y));//右上
	pDC->LineTo(ROUND(P[1].x),ROUND(P[1].y));//闭合	
	pDC->Arc(ROUND(P[9].x),ROUND(P[9].y),ROUND(P[10].x),ROUND(P[10].y),ROUND(P[6].x),ROUND(P[6].y),ROUND(P[5].x),ROUND(P[5].y));
	pDC->Arc(ROUND(P[12].x),ROUND(P[12].y),ROUND(P[11].x),ROUND(P[11].y),ROUND(P[8].x),ROUND(P[8].y),ROUND(P[7].x),ROUND(P[7].y));
	pDC->SelectObject(pOldPen);
	NewPen.DeleteObject();
	pDC->SelectObject(pOldBrush);
	NewBrush.DeleteObject();
	ReleaseDC(pDC);
}


void CTestView::OnMstyle()//设计地砖样式
{
	// TODO: Add your command handler code here
	RedrawWindow();
	ReadPoint();
	DrawObject();	
}

void CTestView::ReadPoint()
{
	P[0]=CP2(0,0);//正方形中心点
	P[1]=CP2(P[0].x-HalfEdge,P[0].y+HalfEdge);//正方形左上角点
	P[2]=CP2(P[0].x-HalfEdge,P[0].y-HalfEdge);//正方形左下角点
	P[3]=CP2(P[0].x+HalfEdge,P[0].y-HalfEdge);//正方形右下角点
	P[4]=CP2(P[0].x+HalfEdge,P[0].y+HalfEdge);//正方形右上角点
	P[5]=CP2(P[0].x-HalfEdge,P[0].y);//正方形左边中点
	P[6]=CP2(P[0].x,P[0].y-HalfEdge);//正方形下边中点
	P[7]=CP2(P[0].x+HalfEdge,P[0].y);//正方形右边中点
	P[8]=CP2(P[0].x,P[0].y+HalfEdge);//正方形上边中点
	P[9]=CP2(P[0].x-2*HalfEdge,P[0].y);//弧外接矩形角点
	P[10]=CP2(P[0].x,P[0].y-2*HalfEdge);//弧外接矩形角点
	P[11]=CP2(P[0].x+2*HalfEdge,P[0].y);//弧外接矩形角点
	P[12]=CP2(P[0].x,P[0].y+2*HalfEdge);//弧外接矩形角点
}
void CTestView::OnMlay() //铺设地砖
{
	// TODO: Add your command handler code here
	tran.SetMat(P,13);
	tran.Translate(-2*HalfEdge,2*HalfEdge);
	DrawObject();//上左
	tran.Translate(2*HalfEdge,-2*HalfEdge);

	tran.Rotate(90,P[0]);
	tran.Translate(0,2*HalfEdge);
	DrawObject();//上中
	tran.Translate(0,-2*HalfEdge);

	tran.Rotate(90,P[0]);
	tran.Translate(2*HalfEdge,2*HalfEdge);
	DrawObject();//上右
	tran.Translate(-2*HalfEdge,-2*HalfEdge);

	tran.Rotate(90,P[0]);
	tran.Translate(-2*HalfEdge,0);
	DrawObject();//中左
	tran.Translate(2*HalfEdge,0);

	tran.Rotate(90,P[0]);
	DrawObject();//中中

	tran.Rotate(90,P[0]);
	tran.Translate(2*HalfEdge,0);
	DrawObject();//中右
	tran.Translate(-2*HalfEdge,0);

	tran.Rotate(90,P[0]);
	tran.Translate(-2*HalfEdge,-2*HalfEdge);
	DrawObject();//下左
	tran.Translate(2*HalfEdge,2*HalfEdge);
	
	tran.Rotate(90,P[0]);
	tran.Translate(0,-2*HalfEdge);
	DrawObject();//下中
	tran.Translate(0,2*HalfEdge);

	tran.Rotate(90,P[0]);
	tran.Translate(2*HalfEdge,-2*HalfEdge);
	DrawObject();//下右
	tran.Translate(-2*HalfEdge,2*HalfEdge);
}
