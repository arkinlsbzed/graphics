
// TestView.cpp : CTestView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "Test.h"
#endif

#include "TestDoc.h"
#include "TestView.h"
#include "math.h"//包含数学头文件
#define  PI 3.1415926//PI的宏定义
#define Round(d) int(floor(d+0.5))//四舍五入宏定义
#define N_MAX_POINT 21//控制多边形的最大顶点数
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_COMMAND(IDM_DRAWPIC, &CTestView::OnDrawpic)
//	ON_WM_LBUTTONDOWN()
//	ON_WM_LBUTTONUP()
//	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()

// CTestView 构造/析构

CTestView::CTestView()
{
	// TODO: 在此处添加构造代码
	flag=true;
}

CTestView::~CTestView()
{
	
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CTestView 绘制

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
	DoubleBuffer(pDC);
}


// CTestView 打印

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CTestView 诊断

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG


// CTestView 消息处理程序


void CTestView::OnDrawpic() 
{
	// TODO: Add your command handler code here
	flag=false;
	Invalidate(FALSE);
	
}
void CTestView::ReadPoint()//读入控制多边形16个顶点坐标
{
	P3[0][0].x=20;  P3[0][0].y=0;  P3[0][0].z=200;//P00
	P3[0][1].x=0;   P3[0][1].y=100;P3[0][1].z=150;//P01
	P3[0][2].x=-130;P3[0][2].y=100;P3[0][2].z=50; //P02
	P3[0][3].x=-250;P3[0][3].y=50; P3[0][3].z=0;  //P03

	P3[1][0].x=100; P3[1][0].y=100;P3[1][0].z=150;//P10
	P3[1][1].x=30;  P3[1][1].y=100;P3[1][1].z=100;//p11
	P3[1][2].x=-40; P3[1][2].y=100;P3[1][2].z=50; //p12
	P3[1][3].x=-110;P3[1][3].y=100;P3[1][3].z=0;  //p13

	P3[2][0].x=280; P3[2][0].y=90; P3[2][0].z=140;//P20
	P3[2][1].x=110; P3[2][1].y=120;P3[2][1].z=80; //P21
	P3[2][2].x=30;  P3[2][2].y=130;P3[2][2].z=30; //P22
	P3[2][3].x=-100;P3[2][3].y=150;P3[2][3].z=-50;//P23

	P3[3][0].x=350; P3[3][0].y=30; P3[3][0].z=150;//P30
	P3[3][1].x=200; P3[3][1].y=150;P3[3][1].z=50; //P31
	P3[3][2].x=50;  P3[3][2].y=200;P3[3][2].z=0;  //P32
	P3[3][3].x=0;   P3[3][3].y=100;P3[3][3].z=-70;//P33	
}

void CTestView::DoubleBuffer(CDC *pDC)//双缓冲
{
	CRect rect;//定义客户区矩形
	GetClientRect(&rect);//获得客户区的大小
	pDC->SetMapMode(MM_ANISOTROPIC);//pDC自定义坐标系
	pDC->SetWindowExt(rect.Width(),rect.Height());//设置窗口范围
	pDC->SetViewportExt(rect.Width(),-rect.Height());//设置视区范围,x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);//客户区中心为原点
	CDC memDC;//内存DC
	CBitmap NewBitmap,*pOldBitmap;//内存中承载的临时位图
	memDC.CreateCompatibleDC(pDC);//创建一个与显示pDC兼容的内存memDC 
	NewBitmap.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height());//创建兼容位图 
	pOldBitmap=memDC.SelectObject(&NewBitmap);//将兼容位图选入memDC 
	memDC.FillSolidRect(rect,pDC->GetBkColor());//按原来背景填充客户区，否则是黑色
	memDC.SetMapMode(MM_ANISOTROPIC);//memDC自定义坐标系
	memDC.SetWindowExt(rect.Width(),rect.Height());
	memDC.SetViewportExt(rect.Width(),-rect.Height());
	memDC.SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);
	DrawCtrlPolygon(&memDC);//绘制控制多边形
	SignCtrPoint(&memDC);//标注控制点编号
	DrawObject(&memDC);//绘制B样条曲面
	pDC->BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&memDC,-rect.Width()/2,-rect.Height()/2,SRCCOPY);//将内存memDC中的位图拷贝到显示pDC中
	memDC.SelectObject(pOldBitmap);//恢复位图
	NewBitmap.DeleteObject();//删除位图
}
void CTestView::ObliqueProjection()//斜等测投影
{
	for(int i=0;i<4;i++)
		for(int j=0;j<4;j++)
		{
			P2[i][j].x=P3[i][j].x-P3[i][j].z/sqrt(2.0);
			P2[i][j].y=P3[i][j].y-P3[i][j].z/sqrt(2.0);
		}
}

void CTestView::DrawCtrlPolygon(CDC *pDC)//绘制控制多边形
{
	ReadPoint();
	ObliqueProjection();
	CPen NewPen,*pOldPen;
	NewPen.CreatePen(PS_SOLID,3,RGB(0,0,0));
	pOldPen=pDC->SelectObject(&NewPen);
	for(int i=0;i<4;i++)
	{
		pDC->MoveTo(Round(P2[i][0].x),Round(P2[i][0].y));
		for(int j=1;j<4;j++)
			pDC->LineTo(Round(P2[i][j].x),Round(P2[i][j].y));
	}
	for(int j=0;j<4;j++)
	{
		pDC->MoveTo(Round(P2[0][j].x),Round(P2[0][j].y));
		for(int i=1;i<4;i++)
			pDC->LineTo(Round(P2[i][j].x),Round(P2[i][j].y));
	}
	pDC->SelectObject(pOldPen);
	NewPen.DeleteObject();
}

void CTestView::DrawObject(CDC *pDC)//绘制双三次B样条曲面	
{
	double x,y,u,v,u1,u2,u3,u4,v1,v2,v3,v4;
	double M[4][4];
	M[0][0]=-1;M[0][1]=3;M[0][2]=-3;M[0][3]=1;
	M[1][0]=3;M[1][1]=-6;M[1][2]=3;M[1][3]=0;
	M[2][0]=-3;M[2][1]=0;M[2][2]=3;M[2][3]=0;
	M[3][0]=1;M[3][1]=4;M[3][2]=1;M[3][3]=0;
	LeftMultiMatrix(M,P3);//数字矩阵左乘三维点矩阵
	TransposeMatrix(M);//计算转置矩阵
	RightMultiMatrix(P3,MT);//数字矩阵右乘三维点矩阵
	ObliqueProjection();//斜等测投影
	for(u=0;u<=1;u+=0.1)
		for(v=0;v<=1;v+=0.1)
		{
			u1=u*u*u;u2=u*u;u3=u;u4=1;v1=v*v*v;v2=v*v;v3=v;v4=1;
			x=(u1*P2[0][0].x+u2*P2[1][0].x+u3*P2[2][0].x+u4*P2[3][0].x)*v1
			 +(u1*P2[0][1].x+u2*P2[1][1].x+u3*P2[2][1].x+u4*P2[3][1].x)*v2
			 +(u1*P2[0][2].x+u2*P2[1][2].x+u3*P2[2][2].x+u4*P2[3][2].x)*v3
			 +(u1*P2[0][3].x+u2*P2[1][3].x+u3*P2[2][3].x+u4*P2[3][3].x)*v4;
			y=(u1*P2[0][0].y+u2*P2[1][0].y+u3*P2[2][0].y+u4*P2[3][0].y)*v1
			 +(u1*P2[0][1].y+u2*P2[1][1].y+u3*P2[2][1].y+u4*P2[3][1].y)*v2
			 +(u1*P2[0][2].y+u2*P2[1][2].y+u3*P2[2][2].y+u4*P2[3][2].y)*v3
			 +(u1*P2[0][3].y+u2*P2[1][3].y+u3*P2[2][3].y+u4*P2[3][3].y)*v4;
			x=x/36;y=y/36;
			if(v==0)
				pDC->MoveTo(Round(x),Round(y));
			else
				pDC->LineTo(Round(x),Round(y));
		}		  
	for(v=0;v<=1;v+=0.1)
		for(u=0;u<=1;u+=0.1)
		{
			u1=u*u*u;u2=u*u;u3=u;u4=1;v1=v*v*v;v2=v*v;v3=v;v4=1;
			x=(u1*P2[0][0].x+u2*P2[1][0].x+u3*P2[2][0].x+u4*P2[3][0].x)*v1
			 +(u1*P2[0][1].x+u2*P2[1][1].x+u3*P2[2][1].x+u4*P2[3][1].x)*v2
			 +(u1*P2[0][2].x+u2*P2[1][2].x+u3*P2[2][2].x+u4*P2[3][2].x)*v3
			 +(u1*P2[0][3].x+u2*P2[1][3].x+u3*P2[2][3].x+u4*P2[3][3].x)*v4;
			y=(u1*P2[0][0].y+u2*P2[1][0].y+u3*P2[2][0].y+u4*P2[3][0].y)*v1
			 +(u1*P2[0][1].y+u2*P2[1][1].y+u3*P2[2][1].y+u4*P2[3][1].y)*v2
			 +(u1*P2[0][2].y+u2*P2[1][2].y+u3*P2[2][2].y+u4*P2[3][2].y)*v3
			 +(u1*P2[0][3].y+u2*P2[1][3].y+u3*P2[2][3].y+u4*P2[3][3].y)*v4;
			x=x/36;y=y/36;
			if(u==0)
				pDC->MoveTo(Round(x),Round(y));
			else
				pDC->LineTo(Round(x),Round(y));
		}	
}

void CTestView::LeftMultiMatrix(double M0[][4],CP3 P0[][4])//左乘矩阵M*P
{
	CP3 T[4][4];//临时矩阵
	int i,j;
	for(i=0;i<4;i++)
		for(j=0;j<4;j++)
		{	
			T[i][j].x=M0[i][0]*P0[0][j].x+M0[i][1]*P0[1][j].x+M0[i][2]*P0[2][j].x+M0[i][3]*P0[3][j].x;	
			T[i][j].y=M0[i][0]*P0[0][j].y+M0[i][1]*P0[1][j].y+M0[i][2]*P0[2][j].y+M0[i][3]*P0[3][j].y;
			T[i][j].z=M0[i][0]*P0[0][j].z+M0[i][1]*P0[1][j].z+M0[i][2]*P0[2][j].z+M0[i][3]*P0[3][j].z;
		}
	for(i=0;i<4;i++)
		for(j=0;j<4;j++)
			P3[i][j]=T[i][j];
}

void CTestView::RightMultiMatrix(CP3 P0[][4],double M1[][4])//右乘矩阵P*M
{
	CP3 T[4][4];//临时矩阵
	int i,j;
	for(i=0;i<4;i++)
		for(j=0;j<4;j++)
		{	
			T[i][j].x=P0[i][0].x*M1[0][j]+P0[i][1].x*M1[1][j]+P0[i][2].x*M1[2][j]+P0[i][3].x*M1[3][j];
			T[i][j].y=P0[i][0].y*M1[0][j]+P0[i][1].y*M1[1][j]+P0[i][2].y*M1[2][j]+P0[i][3].y*M1[3][j];
			T[i][j].z=P0[i][0].z*M1[0][j]+P0[i][1].z*M1[1][j]+P0[i][2].z*M1[2][j]+P0[i][3].z*M1[3][j];
		}
	for(i=0;i<4;i++)
		for(j=0;j<4;j++)
			P3[i][j]=T[i][j];
}

void CTestView::SignCtrPoint(CDC *pDC)//标注控制点的编号
{
	if(flag)
	{
		CString str;
		pDC->SetTextColor(RGB(0,0,255));
   		for(int i=0;i<4;i++)
		{
			for(int j=0;j<4;j++)
			{
				str.Format(CString("P%d,%d"),i,j);
				pDC->TextOut(Round(P2[i][j].x+10),Round(P2[i][j].y),str);				
			}		
		}
	}
}

void CTestView::TransposeMatrix(double M0[][4])//转置矩阵
{
	for(int i=0;i<4;i++)
		for(int j=0;j<4;j++)
			MT[j][i]=M0[i][j];	
}



void CTestView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	// TODO: 在此添加专用代码和/或调用基类
	ReadPoint();
}
