
// TestView.h : CTestView 类的接口
//

#pragma once
#include "InputDlg.h"//对话框头文件
#include "StateNode.h"
#include "P2.h"
class CTestView : public CView
{
protected: // 仅从序列化创建
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// 特性
public:
	CTestDoc* GetDocument() const;

// 操作
public:
	void Initial(int);//文法模型函数
	void Peano(double,double);//Peano函数

// 重写
public:

	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	
	CDC *pDC;//设备上下文对象
	CP2 P0;//起点坐标
	CString Axiom[3],Rule[3],NewRule,NewRuleTemp;//绘图规则
	CStateNode Stack[1024];//结点栈
	int	StackPushPos;//结点栈下标
// 生成的消息映射函数
protected:

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDrawpic();
};

#ifndef _DEBUG  // TestView.cpp 中的调试版本
inline CTestDoc* CTestView::GetDocument() const
   { return reinterpret_cast<CTestDoc*>(m_pDocument); }
#endif

