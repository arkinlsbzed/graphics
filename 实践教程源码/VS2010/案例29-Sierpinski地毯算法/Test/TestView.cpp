
// TestView.cpp : CTestView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "Test.h"
#endif

#include "TestDoc.h"
#include "TestView.h"
#include "math.h"//包含数学头文件
#define  PI 3.1415926//PI的宏定义
#define Round(d) int(floor(d+0.5))//四舍五入宏定义
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_COMMAND(IDM_DRAWPIC, &CTestView::OnDrawpic)
END_MESSAGE_MAP()

// CTestView 构造/析构

CTestView::CTestView()
{
	// TODO: 在此处添加构造代码
	pDC=NULL;
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CTestView 绘制

void CTestView::OnDraw(CDC* /*pDC*/)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
}


// CTestView 打印

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CTestView 诊断

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG


// CTestView 消息处理程序


void CTestView::Carpet(int n,CP2 p0,CP2 p1 )//地毯函数
{
	if(0==n)
	{
		FillRectangle(p0,p1);
		return;
	}
	double w=p1.x-p0.x,h=p1.y-p0.y;
	CP2 p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13;
	p2=CP2(p0.x+w/3,p0.y);
	p3=CP2(p0.x+2*w/3,p0.y);
	p4=CP2(p1.x,p0.y+h/3);
	p5=CP2(p1.x,p0.y+2*h/3);
	p6=CP2(p0.x+2*w/3,p1.y);
	p7=CP2(p0.x+w/3,p1.y);
	p8=CP2(p0.x,p0.y+2*h/3);
	p9=CP2(p0.x,p0.y+h/3);
	p10=CP2(p0.x+w/3,p0.y+h/3);
	p11=CP2(p0.x+2*w/3,p0.y+h/3);
    p12=CP2(p0.x+2*w/3,p0.y+2*h/3);
	p13=CP2(p0.x+w/3,p0.y+2*h/3);
	Carpet(n-1,p0,p10);
	Carpet(n-1,p2,p11);
	Carpet(n-1,p3,p4);
	Carpet(n-1,p9,p13);
	Carpet(n-1,p11,p5);
	Carpet(n-1,p8,p7);
	Carpet(n-1,p13,p6);
	Carpet(n-1,p12,p1);
}

void CTestView::FillRectangle(CP2 p0,CP2 p1)
{
	CBrush NewBrush,*pOldBrush;//声明画刷
	NewBrush.CreateSolidBrush(RGB(0,0,0));//创建黑色画刷
	pOldBrush=pDC->SelectObject(&NewBrush);//选入画刷

	pDC->BeginPath();
	pDC->MoveTo(Round(p0.x),Round(p0.y));//绘制正方形
	pDC->LineTo(Round(p1.x),Round(p0.y));
	pDC->LineTo(Round(p1.x),Round(p1.y));
	pDC->LineTo(Round(p0.x),Round(p1.y));
	pDC->LineTo(Round(p0.x),Round(p0.y));
	pDC->EndPath();
	pDC->FillPath();
	pDC->SelectObject(pOldBrush);//恢复保存的画刷
	NewBrush.DeleteObject();//删除新画刷
}

void CTestView::OnDrawpic() 
{
	// TODO: Add your command handler code here
	CInputDlg dlg;
	if(IDOK==dlg.DoModal())
	{
		n=dlg.m_m;
	}
	else
		return;
	RedrawWindow();	
	pDC=GetDC();
	CRect rect;
	GetClientRect(&rect);	
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rect.Width(),rect.Height());
	pDC->SetViewportExt(rect.Width(),-rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);
	CP2 P0,P1;//正方形对角点坐标
	P0=CP2(-rect.Height()/2.0+20,-rect.Height()/2.0+20);
	P1=CP2(rect.Height()/2.0-20,rect.Height()/2.0-20);	
	Carpet(n,P0,P1);
}
